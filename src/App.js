import React from 'react'
import './App.css';
import InputField from './Components/InputField';

function App() {

  const inputRefs = React.useRef(
    [React.createRef(), React.createRef()]
  )

  const [data, setData] = React.useState({})

  const handleChange = (name, value) => {
    setData(prev => ({...prev, [name]:value}))
  }

  const submitForm = (event) =>{
    event.preventDefault();
    
    let isValid = true; 
    
    for (let i = 0; i < inputRefs.current.length; i++) {
      const valid = inputRefs.current[i].current.validate();
      if (!valid) {
        isValid = false
      }
    }
    if (!isValid) {
      return
    }
    console.log("Submitted")
  }

  return (
    <div className="App">
      <form onSubmit={submitForm}>
        <InputField
          name="username"
          label="Username:"
          placeholder="Username"
          onChange={handleChange}
          ref={inputRefs.current[0]}
          validation="required|min:6|max:12"
        /> 

        <InputField
          name="password"
          label="Password:"
          placeholder="Password"
          type="password"
          onChange={handleChange}
          ref={inputRefs.current[1]}
          validation="required|min:6"
        />

        <button type="submit">Login</button>
        
      </form>
    </div>
  );
}

export default App;
